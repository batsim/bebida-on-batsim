#!/usr/bin/env perl

my $input = q(EXTRAE_Paraver_trace_mpich); # input file name

use strict;
use warnings;
use Data::Dumper;
use Switch;

# use dictionary to keep track of states and events
my %states;
my %events;

# use dictionary to keep track of translated and ignored events
my %translated_events;
my %ignored_events;

my $number_of_tasks;

# store communicator id and size
my %communicators;

my @task_states_buffer;
my @task_events_buffer;
my @task_comms_buffer;

my $power_reference = 16.673; # in flop/µs taken from graphene simgrid platform

# store all tit events (complete or incomplete)
my @action_buffer;

# to define the root of collective operations (bcast, gather)
my %collective_root; # communicator(HASH), task(HASH), mpi_call(HASH) - ARRAY
my %collective_root_order; # communicator(HASH), mpi_call(HASH) - ARRAY

# to define recv/send counts of collective operations
my %v_counts;  # communicator(HASH), mpi_call(HASH) then one ARRAY (indicating order of operations) of ARRAYs (with the message sizes)
my %v_actions; # communicator(HASH), task(HASH), mpi_call(HASH), 

# to define the partner and comm size of ptp operations (send, recv, isend, irecv)
my %ptp_partner_comm; # task(HASH), "send" or "recv" - ARRAY
my @ptp_operations_order; # order on which PTP operations appear

sub check_undef_action
{
    my $action = @_;
    foreach my $value ($action){
        if (!defined $value){
            return undef;
        }
    }
    return 1;
}

sub dump_tit_lucas
{
    define_collective_root ();
    define_ptp_partner_comm_size ();
    define_v_fields ();

    foreach (@action_buffer){
        my $action = $_;
        my $type = $action->{"type"};
        my $task = $action->{"task"};
        $task = $task - 1; # remove one

        # check if action has no undefs
        if (!check_undef_action ($action)){
            die "action has undefs\n";
        }

        switch ($type){
            case ["compute"] {
                my $comp_size = $action->{"comp_size"};
                print "$task $type $comp_size\n";
            }

            case ["init", "finalize", "wait", "waitall", "barrier"] {
                # FORMAT: <rank> init [<set_default_double>]
                # FORMAT: <rank> finalize
                # FORMAT: <rank> wait
                # FORMAT: <rank> waitAll
                # FORMAT: <rank> barrier
                print "$task $type\n";
            }

            case ["bcast"] {
                # FORMAT: <rank> bcast <comm_size> [<root> [<datatype>]]
                my $comm_size = $action->{"comm_size"};
                my $root = $action->{"root"} - 1;
                print "$task $type $comm_size $root\n";
            }

            case ["gather"] {
                # FORMAT: <rank> gather <send_size> <recv_size> <root> [<send_datatype> <recv_datatype>]
                my $send_size = $action->{"send_size"};
                my $recv_size = $action->{"recv_size"};
                my $root = $action->{"root"} - 1;
                print "$task $type $send_size $recv_size $root\n";
            }

            case ["reduce"] {
                # FORMAT: <rank> reduce <comm_size> <comp_size> [<root> [<datatype>]]
                my $comm_size = $action->{"comm_size"};
                my $comp_size = $action->{"comp_size"};
                my $root = $action->{"root"} - 1;
                print "$task $type $comm_size $comp_size $root\n";
            }

            case ["allreduce"] {
                # FORMAT: <rank> allReduce <comm_size> <comp_size> [<datatype>]
                my $comm_size = $action->{"comm_size"};
                my $comp_size = $action->{"comp_size"};
                print "$task $type $comm_size $comp_size\n";
            }

            case ["send", "recv", "isend", "irecv"] {
                # FORMAT: <rank> send <dst> <comm_size> [<datatype>]
                my $partner = $action->{"partner"} - 1;
                my $comm_size = $action->{"comm_size"};
                print "$task $type $partner $comm_size\n";
            }

            case ["allgather", "alltoall"] {
                # FORMAT: <rank> allGather <send_size> <recv_size> [<send_datatype> <recv_datatype>]
                # FORMAT: <rank> allToAll <send_size> <recv_recv> [<send_datatype> <recv_datatype>]
                my $send_size = $action->{"send_size"};
                my $recv_size = $action->{"recv_size"};
                print "$task $type $send_size $recv_size\n";
            }

            case ["gatherv"] {
                # FORMAT: <rank> gatherV <send_size> <recv_sizes†> <root> [<send_datatype> <recv_datatype>]
                my $send_size = $action->{"send_size"};
                my $recv_sizes = join(" ", @{$action->{"recv_sizes"}});
                my $root =  $action->{"root"} - 1;
                print "$task $type $send_size $recv_sizes $root\n";
            }

            case ["allgatherv"] {
                # FORMAT: <rank> allGatherV <send_size> <recv_sizes†> [<send_datatype> <recv_datatype>]
                my $send_size = $action->{"send_size"};
                my $recv_sizes = join(" ", @{$action->{"recv_sizes"}});
                print("$task $type $send_size $recv_sizes\n");
            }

            case ["reducescatter"] {
                # FORMAT: <rank> reduceScatter <recv_sizes†> <comp_size> [<datatype>]
                my $recv_sizes = join(" ", @{$action->{"recv_sizes"}});
                my $comp_size = $action->{"comp_size"};
                print("$task $type $recv_sizes $comp_size\n");
            }
            case ["alltoallv"] {
                # FORMAT: <rank> alltoallv <rec_size> <recv_sizes†> <send_size> <send_sizes†> [<send_datatype> <recv_datatype>]
                # print("ACTION");
                # print Dumper($action);
                # my $send_size = $action->{"send_size"};
                # my $send_sizes = join(" ", @{$action->{"send_sizes"}});
                # my $recv_size = $action->{"recv_size"};
                # my $recv_sizes = join(" ", @{$action->{"recv_sizes"}});
                # print("$task $type $send_size $send_sizes $recv_size $recv_sizes\n");
                # FIXME: replace alltoallv by an alltoall because we don't have the information send and recv vector details in the traces
                my $send_size = $action->{"send_size"};
                my $recv_size = $action->{"recv_size"};
                print "$task alltoall $send_size $recv_size\n";
            }
            default {
                die "<missing treatment of ", "$type>\n";
            }
        }
    }
}


sub define_v_fields
{
    # hash of communicators
    for my $comm (keys %v_actions) {
        # print "=> $comm\n";
        # hash of tasks
        for my $task (keys %{$v_actions{$comm}}){
            # print "  => $task\n";
            # hash of mpicalls
            for my $mpi_call (keys %{$v_actions{$comm}{$task}}){
                # print "    => $mpi_call\n";
                # array of requests (which are HASHes)
                my $index = 0;
                for my $elem (@{$v_actions{$comm}{$task}{$mpi_call}}){
                    # print "[$index]       => $elem\n";
                    # print Dumper($v_counts{$comm}{$mpi_call}[$index]);
                    $elem->{"recv_sizes"} = @{$v_counts{$comm}{$mpi_call}}[$index];
                    # print Dumper($elem);
                    $index++;
                }
            }
        }
    }
}

sub define_each_ptp_partner_comm_size
{
    my($task_send, $task_recv, $comm_size) = @_;

    # deal with send action
    my $send_action = \%{@{$ptp_partner_comm{$task_send}{"send"}}[0]};
    $send_action->{"partner"} = $task_recv;
    $send_action->{"comm_size"} = $comm_size;
    shift @{$ptp_partner_comm{$task_send}{"send"}};

    # deal with recv action
    my $recv_action = \%{@{$ptp_partner_comm{$task_recv}{"recv"}}[0]};
    $recv_action->{"partner"} = $task_send;
    $recv_action->{"comm_size"} = $comm_size;
    shift @{$ptp_partner_comm{$task_recv}{"recv"}};
    return;

}

sub define_ptp_partner_comm_size
{
    for my $elem (@ptp_operations_order){
        my $send = $elem->{"task_send"};
        my $recv = $elem->{"task_recv"};
        my $size = $elem->{"comm_size"};
        define_each_ptp_partner_comm_size ($send, $recv, $size);
    }

    # print "====\n";
    # print Dumper(@ptp_operations_order);
    # print "===========\n";
    # print Dumper(%ptp_partner_comm);
    # print "====\n";
    return;

}

sub define_collective_root
{
    # hash of communicators
    for my $comm (keys %collective_root) {
        # hash of tasks
        for my $task (keys %{$collective_root{$comm}}){
            # hash of mpicalls
            for my $mpi_call (keys %{$collective_root{$comm}{$task}}){
                # array of requests (which are HASHes)
                my $index = 0;
                for my $elem (@{$collective_root{$comm}{$task}{$mpi_call}}){
                    $elem->{'root'} = $collective_root_order{$comm}{$mpi_call}[$index];
                    $index++;
                }
            }
        }
    }
    return;
}

sub main {
    my($arg);

    while(defined($arg = shift(@ARGV))) {
        for ($arg) {
            if (/^-i$/) { $input = shift(@ARGV); last; }
            print "unrecognized argument '$arg'\n";
        }
    }
    if(!defined($input) || $input eq "") { die "No valid input file provided.\n"; }

    parse_pcf($input.".pcf");
    parse_prv_lucas ($input.".prv");

    dump_tit_lucas();

    print STDERR "Translated events:\n";
    print STDERR join ", ", keys %translated_events;
    print STDERR "\nIgnored events:\n";
    print STDERR join ", ", keys %ignored_events;
    print STDERR "\n";
}

my %mpi_call_parameters = (
    "send size" => "50100001",
    "recv size" => "50100002",
    "root" => "50100003",
    "communicator" => "50100004",
);

my @mpi_calls = (
    "MPI_Finalize",       #
    "MPI_Init",           #
    "MPI_Send",           #
    "MPI_Recv",           #
    "MPI_Isend",          #
    "MPI_Irecv",          #
    "MPI_Wait",           #
    "MPI_Waitall",        #
    "MPI_Bcast",          #
    "MPI_Reduce",         #
    "MPI_Allreduce",      #
    "MPI_Barrier",        #
    "MPI_Gather",         #
    "MPI_Allgather",      #
    "MPI_Alltoall",       #
    "MPI_Gatherv",        #
    "MPI_Allgatherv",     #
    "MPI_Reduce_scatter", #
    "MPI_Alltoallv"
    );


# search for a MPI call in the event's parameters
# in all the cases I have seen, the event type and value are the first
# numbers in the event's parameter list, however we are not making this
# assumption. Instead, we look at all parameters and search for the one
# that is encoding the MPI call
sub extract_mpi_call {
    my %event_info = @_;

    # search for a MPI call in the event's parameters
    foreach my $key (keys %event_info) {
        if(defined($events{$key})) {
            if(defined($events{$key}{value}{$event_info{$key}})) {
                my $event_name = $events{$key}{value}{$event_info{$key}};
                if(grep(/^$event_name$/, @mpi_calls)) {
                    $translated_events{$event_name} = 1;
                    return $event_name;
                }
                else {
                    $ignored_events{$event_name} = 1;
                }
            }
        }
    }
    return "None";
}

sub register_v_count {
    my ($comm, $mpi_call, $task, $send_size) = @_;
#   print "Operation $mpi_call found, have to add $send_size to position $task of recv sizes on $comm/$mpi_call\n";

    # -1 because paraver does not start at zero
    $task--;

    # array (indicating order of operations) of arrays (with the message sizes)
    if (!defined $v_counts{$comm}{$mpi_call}){
        # only the first time, when the array of operations does not exist
        my @ar;
        @ar[$task] = $send_size;
        push @{$v_counts{$comm}{$mpi_call}}, \@ar;
    }
    else {
        # ok, operation order array already exists, search for array to put value $send_size at $task position
        # in other words, I have to look for the first array where $task position is undef

        my $found = 0;
        my $index = 0;
        for my $elem (@{$v_counts{$comm}{$mpi_call}}){
            if (!defined @{$elem}[$task]){
                # print "Position $task is not defined.\n";
                @{$elem}[$task] = $send_size;
                $found = 1;
                last;
            }
            $index++;
        }
        if (!$found){
            # print ("Go to the end of the array of operations and were unabled to find a position, create a new one.\n");
            my @ar;
            @ar[$task] = $send_size;
            push @{$v_counts{$comm}{$mpi_call}}, \@ar;
        }
    }
}


sub register_root
{
    my ($comm, $task, $mpi_call, $root, $action) = @_;

    # generic way of dealing with root (for those tit events that needs it)
    if (defined $root){
        $action->{"root"} = $task;
        push @{$collective_root_order{$comm}{$mpi_call}}, $task;
    }
    push @{$collective_root{$comm}{$task}{$mpi_call}}, $action;
}

sub parse_prv_lucas
{
    my($prv) = @_; # get arguments
    open(INPUT, $prv) or die "Cannot open $prv. $!";

    # check if header is valid, we should get something like #Paraver (dd/mm/yy at hh:m):ftime:0:nAppl:applicationList[:applicationList]
    my $line = <INPUT>;
    chomp $line;
    $line =~ /^\#Paraver / or die "Invalid header '$line'\n";
    my $header = $line;
    $header =~ s/^[^:\(]*\([^\)]*\):// or die "Invalid header '$line'\n";
    $header =~ s/(\d+):(\d+)([^\(\d])/$1\_$2$3/g;
    $header =~ s/,\d+$//g;
    my($max_duration, $resource, $number_of_apps, @app_info_list) = split(/:/, $header);
    $max_duration =~ s/_.*$//g;
    $resource =~ /^(.*)\((.*)\)$/ or die "Invalid resource description '$resource'\n";
    my($number_of_nodes, $node_cpu_count) = ($1, $2);
    $number_of_apps == 1 or die "Only one application can be handled at the moment\n";
    my @node_cpu_count = split(/,/, $node_cpu_count);

    # parse app info
    foreach my $app (1..$number_of_apps) {
        $app_info_list[$app - 1] =~ /^(.*)\((.*)\)$/ or die "Invalid application description\n";
        my $task_info;
        ($number_of_tasks, $task_info) = ($1, $2);
        my(@task_info_list) = split(/,/, $task_info);

        # initiate an empty event buffer for each task
        @task_events_buffer = (0);
        foreach my $task (1..$number_of_tasks) {
            my($number_of_threads, $node) = split(/_/, $task_info_list[$task - 1]);
            my @buffer;
            $task_events_buffer[$task - 1] = \@buffer;
        }
    }

    # LUCAS version LUCAS
    # start reading records
    while(defined($line=<INPUT>)) {
        chomp $line;

        # state records are in the format 1:cpu:appl:task:thread:begin_time:end_time:state_id
        # they keep states along time for each cpu/appl/task/thread
        if($line =~ /^1/) {
            my($record, $cpu, $appli, $task, $thread, $begin_time, $end_time, $state_id) = split(/:/, $line);
            my $state_name = $states{$state_id}{name};

            if ($state_name eq "Running"){
                my $comp_size = ($end_time - $begin_time) * $power_reference;

                my %action_compute;
                $action_compute{"type"} = "compute";
                $action_compute{"comp_size"} = $comp_size;
                $action_compute{"task"} = $task;
                push @action_buffer, \%action_compute;
            }
        }

        # event records are in the format 2:cpu:appl:task:thread:time:event_type:event_value
        # => multiple event_type:event_value might be present
        # these keep a number of events (MPI or others)
        # LUCAS
        elsif($line =~ /^2/){
            my($record, $cpu, $appli, $task, $thread, $time, %event_list) = split(/:/, $line);
            my $mpi_call = extract_mpi_call(%event_list);

            if($mpi_call ne "None") {

                # the action to be pushed (a HASH)
                my %action;

                # start preparation of the task
                # define the task and its type
                $action{"task"} = $task;
                my $teste;
                $teste = $mpi_call;
                $teste =~ s/MPI_//;
                $teste = lc($teste);
                $action{"type"} = $teste;

                # extract information from this paraver event
                my $event_list;
                my $send_size = $event_list{$mpi_call_parameters{"send size"}};
                my $recv_size = $event_list{$mpi_call_parameters{"recv size"}};
                my $root = $event_list{$mpi_call_parameters{"root"}}; #marker present in the root
                my $comm = $event_list{$mpi_call_parameters{"communicator"}};

                # register root
                switch ($mpi_call) {
                    case ["MPI_Bcast", "MPI_Gather", "MPI_Reduce", "MPI_Gatherv"] {
                        $action{"root"} = undef;

                        # register the root for backpatching
                        register_root ($comm, $task, $mpi_call, $root, \%action);
                    }
                }

                # register ptp info
                switch ($mpi_call){
                    case ["MPI_Send", "MPI_Isend"] {
                        push @{$ptp_partner_comm{$task}{"send"}}, \%action;
                    }

                    case ["MPI_Recv", "MPI_Irecv"] {
                        push @{$ptp_partner_comm{$task}{"recv"}}, \%action;
                    }
                }

                # remaining parameters
                switch ($mpi_call) {
                    case ["MPI_Init", "MPI_Finalize"] {
                        # nothing particular to do
                    }

                    case ["MPI_Bcast"] {
                        $action{"comm_size"} = $send_size > $recv_size ? $send_size : $recv_size;
                    }

                    case ["MPI_Send", "MPI_Recv", "MPI_Isend", "MPI_Irecv"] {
                        $action{"partner"} = undef;
                        $action{"comm_size"} = undef;
                    }

                    case ["MPI_Allreduce", "MPI_Reduce"] {
                        $action{"comm_size"} = $send_size > $recv_size ? $send_size : $recv_size; # TODO
                        $action{"comp_size"} = 0;     # where should we take this information from?
                    }

                    case ["MPI_Gather", "MPI_Allgather", "MPI_Alltoall"] {
                        $action{"send_size"} = $send_size;
                        $action{"recv_size"} = $recv_size;
                    }

                    case ["MPI_Allgatherv", "MPI_Gatherv"] {
                        $action{"send_size"} = $send_size;
                        $action{"recv_sizes"} = undef;

                        # the action that should be backpatched
                        push @{$v_actions{$comm}{$task}{$mpi_call}}, \%action;

                        # the information that should be used to backpatch
                        register_v_count ($comm, $mpi_call, $task, $send_size);
                    }

                    case ["MPI_Reduce_scatter"] {
                        $action{"comp_size"} = 0;     # where should we take this information from?

                        # the action that should be backpatched
                        push @{$v_actions{$comm}{$task}{$mpi_call}}, \%action;

                        # the information that should be used to backpatch
                        register_v_count ($comm, $mpi_call, $task, $recv_size);
                    }

                    case["MPI_Alltoallv"] {

                        # print("EVENT");
                        # print Dumper($line);
                        $action{"send_size"} = $send_size / $number_of_tasks;
                        $action{"send_sizes"} = undef;
                        $action{"recv_size"} = $recv_size / $number_of_tasks;
                        $action{"recv_sizes"} = undef;

                        # the action that should be backpatched
                        push @{$v_actions{$comm}{$task}{$mpi_call}}, \%action;

                        # the information that should be used to backpatch
                        register_v_count ($comm, $mpi_call, $task, $send_size);
                    }
                }
                # push to the action array (everything is buffered before dump at the end)
                push @action_buffer, \%action;
            }
        }
        # communication records are in the format 3:cpu_send:ptask_send:task_send:thread_send:logical_time_send:actual_time_send:cpu_recv:ptask_recv:task_recv:thread_recv:logical_time_recv:actual_time_recv:size:tag
        elsif($line =~ /^3/) {
            my($record, $cpu_send, $ptask_send, $task_send, $thread_send, $ltime_send, $atime_send, $cpu_recv, $ptask_recv, $task_recv, $thread_recv, $ltime_recv, $atime_recv, $size, $tag) = split(/:/, $line);
         # get mpi call parameters from the communication entry

            my %ptp_operation;
            $ptp_operation{"task_send"} = $task_send;
            $ptp_operation{"task_recv"} = $task_recv;
            $ptp_operation{"comm_size"} = $size;
            push @ptp_operations_order, \%ptp_operation;
        }
        next;

        # communicator record are in the format c:app_id:communicator_id:number_of_process:thread_list (e.g., 1:2:3:4:5:6:7:8)
        if($line =~ /^c/) {
            my($record, $appli, $id, $size, @task_list) = split(/:/, $line);
            $communicators{$id} = { 'size' => $size };
            $communicators{$id}{tasks} = [@task_list];
        }
    }

    # for my $task (1 .. $number_of_tasks) {
    #     generate_tit($task);
    # }

    return;
}

sub parse_pcf {
    my($pcf) = shift; # get first argument
    my $line;

    open(INPUT, $pcf) or die "Cannot open $pcf. $!";
    while(defined($line=<INPUT>)) {
        chomp $line; # remove new line
        if($line =~ /^STATES$/) {
            while((defined($line=<INPUT>)) && ($line =~ /^(\d+)\s+(.*)/g)) {
                $states{$1}{name} = $2;
    $states{$1}{used} = 0;
            }
        }

        if($line =~ /^EVENT_TYPE$/) {
      my $id;
            while($line=<INPUT>) { # read event
                if($line =~ /VALUES/g) {
        while((defined($line=<INPUT>)) && ($line =~ /^(\d+)\s+(.*)/g)) { # read event values
      $events{$id}{value}{$1} = $2;
        }
        last;
    }
                $line =~ /[\d]\s+(\d+)\s+(.*)/g or next;
                $id = $1;
                $events{$id}{type} = $2;
    $events{$id}{used} = 0;
            }
        }
    }

    #print Dumper(\%states);
    #print Dumper(\%events);
}

main();
