# Time Independent Traces generation

To generate traces for the NAS Parallel Benchmarks, run the notebook with the
following command.

## On grid5000

Take a machine with a lot of ram:
```sh
# localy
ssh -A grenoble.g5k
```

```sh
# Now on the frontend
oarsub -p "cluster='dahu'" -I -l "nodes=1,walltime=3:50:00"
```

Create a tunnel:
```sh
ssh -D 5000 -A grenoble.g5k
```

Then setup the sock proxy of sour browser to localhost:5000

```sh
# get root access
sudo-g5k

# Install nix
curl https://nixos.org/nix/install | sh

# Fix grid5000 misconfiguration
export XDG_RUNTIME_DIR=/tmp/nix-env

. /home/$USER/.nix-profile/etc/profile.d/nix.sh

# install tools
nix-env -f https://github.com/oar-team/kapack/archive/e46856b0c571b2eacbe4afc466c1106e73c62670.tar.gz -iA pkgs.parallel pkgs.perl pkgs.perlPackages.Switch

export PERL5LIB=$(nix-build https://github.com/oar-team/kapack/archive/e46856b0c571b2eacbe4afc466c1106e73c62670.tar.gz --no-build-output -A pkgs.perlPackages.Switch)/lib/perl5/site_perl/5.24.3/:$PERL5LIB

# start jupyter
nix-shell https://github.com/oar-team/kapack/archive/7f479467216693be045a80403f00291b478a36e9.tar.gz \
  -A evalysNotebookEnv
```

Click on the link that is given by jupyter link.

Final traces that where generated this way are located in ./results/final
