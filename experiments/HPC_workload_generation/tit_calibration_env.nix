{ kapack ? import (fetchTarball https://github.com/oar-team/kapack/archive/57f1609c5be5c67226727a0f7cf4a64bbee7d5b0.tar.gz) {}
#~/Projects/kapack {}
}:

let
  inherit (kapack) pkgs python batsim_dev;
in
  pkgs.stdenv.mkDerivation rec {
    name = "TIT_calibration_env";
    env = pkgs.buildEnv { name = name; paths = buildInputs; };
    buildInputs = [
      (python.withPackages(ps : with ps; [ pandas ipython matplotlib jupyter ]))
      batsim_dev
    ];
  }
