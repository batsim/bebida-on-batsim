#!/usr/bin/env bash

sudo-g5k

curl https://nixos.org/nix/install | sh

# Fix grid5000 misconfiguration
export XDG_RUNTIME_DIR=/tmp/nix-env

. /home/$USER/.nix-profile/etc/profile.d/nix.sh

nix-env -f https://github.com/oar-team/kapack/archive/master.tar.gz -iA npb pkgs.openmpi extrae
