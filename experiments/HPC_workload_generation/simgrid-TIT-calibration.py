#! /usr/bin/env nix-shell
#! nix-shell tit_calibration_env.nix -i "ipython --pdb"
"""
This script is made to calibrate TIT traces by scaling the compute values of a
TIT trace using an iterative convergence process using Batsim simulations.
"""

import concurrent.futures
import datetime
import json
import multiprocessing
# import csv
import os
import pathlib
import shlex
import shutil
import subprocess
import sys
from typing import Optional

import pandas


def scale_compute(trace_path, base_path, output_path, scale_factor):
    old_file_path = os.path.join(base_path, trace_path)
    # print(f"Calibrating trace: {old_file_path}")
    with open(old_file_path) as trace_file:

        new_file_path = os.path.join(output_path, trace_path)
        pathlib.Path(os.path.dirname(new_file_path)).mkdir(parents=True, exist_ok=True)
        #if scale_factor < 0.5:
        #    scale_factor = scale_factor 
        #
        #elif scale_factor > 2:


        with open(new_file_path, "w") as new_trace:
            for line_num, line in enumerate(trace_file.readlines()):
                # print(line)
                # Do nothing when it is a comment
                if line.startswith("#"):
                    continue

                new_line = None

                split_line = line.strip().split(" ")
                mpi_call = split_line[1]

                if mpi_call == "compute":
                    split_line[2] = str(float(split_line[2]) * scale_factor)
                    new_line = " ".join(split_line)

                if new_line is not None:
                    # print("line: "+ str(line_num) + " in file " + old_file_path +
                    #        " processed\n:OLD: " + line + "\n:NEW: " + new_line)
                    new_trace.write(new_line + "\n")
                else:
                    new_trace.write(line)
    # print(f"Calibrated trace: {new_file_path}")


def scale_compute_on_TITs(
    input_trace_list_file_path: str, output_traces_path: str, scale_factor: float
):

    # creates results dir
    try:
        pathlib.Path(output_traces_path).mkdir(parents=True, exist_ok=True)
    except FileExistsError:
        print(f"WARNING: Output directory {output_traces_path} already exists")

    # copy trace list file
    try:
        shutil.copy(input_trace_list_file_path, output_traces_path)
    except shutil.SameFileError:
        print(
            "ERROR: Inplace replacement of the trace is not supported: "
            "Please, select another output path"
        )
        sys.exit(-1)

    with open(input_trace_list_file_path) as tracelist_file:
        trace_list = tracelist_file.readlines()

    # get based path relative to trace list file
    base_path = os.path.dirname(input_trace_list_file_path)

    trace_list = [x.strip() for x in trace_list]

    # process trace files
    for trace_path in trace_list:
        if os.path.isabs(trace_path):
            sys.exit("ERROR: Absolute path in the trace list file is not supported")
        scale_compute(trace_path, base_path, output_traces_path, scale_factor)


def generate_workload(
    app_name: str, app_profile: dict, nb_proc_per_nodes: int, suffix: str = ""
):
    res = int((app_profile["np"]) / nb_proc_per_nodes)
    wl = {
        "version": 0,
        "description": "one small smpi application for calibration purpose",
        "jobs": [{"id": "1", "subtime": 0, "res": res, "profile": app_name}],
        "nb_res": res,
        "profiles": {app_name: app_profile},
    }
    with open(f"{workloads_dir}/workload_{app_name}_{suffix}.json", "w") as f:
        json.dump(wl, f, indent=4)


def is_under_threshold(scale_factor: Optional[float], threshold: float):
    if scale_factor is None:
        return False
    return scale_factor > (1 - threshold) and scale_factor < (1 + threshold)


def get_calibration_results(
    batsim_output_prefix: str, app_name: str, real_runtime: float, stage: str
):

    df = pandas.read_csv(f"{batsim_output_prefix}_jobs.csv")
    df = df.assign(smpi_app=app_name)
    df = df.assign(real_runtime=real_runtime)
    df = df.assign(stage=stage)
    df = df[
        [
            "smpi_app",
            "real_runtime",
            "execution_time",
            "stage",
        ]
    ]
    df = df.assign(scale_factor=df.real_runtime / df.execution_time)
    # print(df)
    scale_factor = df.scale_factor[0]
    execution_time = df.execution_time[0]
    print(f"New scale factor for {app_name}: {scale_factor}")
    return df, scale_factor, execution_time


def run_cmd(raw_cmd: str):
    cmd = shlex.split(raw_cmd)
    # print(f"starting batsim with the following command: {raw_cmd}")
    subprocess.run(
        cmd, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )
    # os.system(raw_cmd)

    # proc = subprocess.Popen(
    #    cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    # )
    # stdout, stderr = proc.communicate()

    # print(f"[{cmd!r} exited with {proc.returncode}]")
    # if proc.stdout:
    #    print(f"[stdout]\n{stdout.decode()}")
    # if proc.stderr:
    #    print(f"[stderr]\n{stderr.decode()}")


def do_calibration_loop(app_name: str, input_trace_path: str, profile: dict):
    iteration = 0
    scale_factor = None
    input_trace_filename: str = os.path.basename(input_trace_path)
    calib_df = pandas.DataFrame()

    while (iteration < max_iteration) and (
        not is_under_threshold(scale_factor, convergence_threshold)
    ):
        stage = f"stage_{iteration}"

        print(f"Starting calibration of {app_name} stage {iteration}")
        # Generate the workload for this instance
        generate_workload(app_name, profile, nb_proc_per_nodes, suffix=stage)

        # prepare the simulation to get the scale factor
        bat_cmd = f"batsim \
 -p {platform} \
 -w {workloads_dir}/workload_{app_name}_{stage}.json \
 -e {result_dir}/{app_name}_{stage} \
 -v quiet \
 --no-sched \
 --sg-cfg tracing:yes \
 --sg-cfg tracing/uncategorized:yes \
 --sg-cfg tracing/smpi:yes \
 --sg-cfg tracing/smpi/internals:yes \
 --sg-cfg=tracing/filename:{result_dir}/{app_name}_{stage}.paje \
 -r {master_node}:master"

        # Store the command
        with open(f'{result_dir}/batsim_cmd_{app_name}_stage_{iteration}', 'w') as f:
            f.write(bat_cmd)

        # Run batsim
        run_cmd(bat_cmd)

        # get results of the simulation with the scale factor
        df, new_scale_factor, new_execution_time = get_calibration_results(
            f"{result_dir}/{app_name}_{stage}", app_name, profile["runtime"], stage
        )

        # store results
        calib_df = calib_df.append(df, ignore_index=True)
        calib_df.to_csv(f"{result_dir}/{app_name}")

        # Check for stalled calibration
        if scale_factor is not None and new_scale_factor > (scale_factor - 0.005) and new_scale_factor < (scale_factor + 0.005):
            print(f"{app_name} stalled at scale factor {new_scale_factor}!")
            break
        scale_factor = new_scale_factor

        # Prepare next stage...
        iteration = iteration + 1
        stage = f"stage_{iteration}"
        # scale compute in the TITs
        intermediate_output = f"{result_dir}/{app_name}_{stage}"
        scale_compute_on_TITs(input_trace_path, intermediate_output, scale_factor)
        # update profile
        relative_path = os.path.relpath(intermediate_output, start=workloads_dir)
        profile["trace"] = f"{relative_path}/{input_trace_filename}"
        input_trace_path = f"{intermediate_output}/{input_trace_filename}"
        # print(profile)

    print(f"Calibration for {app_name} is finished!")
    # Move final traces to the output path:
    # copy trace list file
    shutil.copy(input_trace_path, output_traces_path + "/" + input_trace_filename)
    with open(input_trace_path) as trace_file:
        # copy files into the result dir
        trace_list = trace_file.readlines()
        for tit_trace_files in trace_list:
            # create trace container dir
            new_traces_dir = os.path.join(output_traces_path,
                                     app_name + "_files")
            pathlib.Path(new_traces_dir).mkdir(parents=True, exist_ok=True)
            old_trace_base_dir = os.path.dirname(input_trace_path)
            old_trace_path = os.path.join(old_trace_base_dir,
                                          tit_trace_files.strip())
            shutil.copy(old_trace_path, new_traces_dir)


    return calib_df


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        "input_traces",
        type=argparse.FileType("r"),
        nargs="+",
        help="List of input TIT traces",
    )

    # parser.add_argument(
    #    "execution_time_CSV",
    #    type=argparse.FileType("r"),
    #    nargs="+",
    #    help="a CSV with two columns: 1st the trace name, 2nd"
    #    " the expected execution time, 3rd number of rank",
    # )
    parser.add_argument(
        "application_profiles_JSON",
        type=argparse.FileType("r"),
        help="""Associated SMPI application profiles which is a dict with
        profile name as key and must contain two fields:
        - `np`: the number of process (ranks)
        - `runtime`: the runtime of the application in seconds

        Example:
        ```js
        {
          "ft.C.16": {
            "np": 16,
            "runtime": 113.8,
          },
          "ft.C.4": {
            "np": 4,
            "runtime": 138.16,
          },
        }
        ```
        """,
    )

    parser.add_argument(
        "nb_proc_per_nodes",
        type=int,
        help="the number of process to allocate to each resources",
    )

    parser.add_argument(
        "platform",
        type=argparse.FileType("r"),
        help="The Simgrid platform that will be used for the simulation",
    )

    parser.add_argument(
        "master_node",
        type=str,
        help="The master node is the Simgrid platform (if needed)",
    )

    parser.add_argument(
        "-o",
        "--output_traces_path",
        type=str,
        default="calibrated_ti_traces",
        help="The path to write calibrated TIT traces",
    )

    parser.add_argument(
        "--max_iteration",
        type=int,
        default="20",
        help="The maximum number of calibration iteration",
    )

    parser.add_argument(
        "--convergence_threshold",
        type=float,
        default="0.05",
        help="This means that the iteration will stop when the ratio of "
        "the real time execution over the simulation execution cross"
        " this threshold",
    )

    args = parser.parse_args()

    # instance_dict = {}
    # csv_reader = csv.reader(args.execution_time_CSV)
    # for row in csv_reader:
    #    if len(row) != 2:
    #        raise Exception(
    #            "Each line of the CSV should contains 2 values: "
    #            "1st the trace name, 2nd"
    #            " the expected execution time"
    #        )
    #    instance_dict[row[0]] = row[1]

    date = datetime.datetime.now().isoformat()
    result_dir = os.path.abspath(f"./tmp-tit-calibration-{date}")
    workloads_dir = os.path.abspath(f"{result_dir}/workloads")

    os.makedirs(result_dir)
    os.makedirs(workloads_dir)

    # Load parameters
    platform = args.platform.name
    master_node = args.master_node
    max_iteration = args.max_iteration
    convergence_threshold = args.convergence_threshold
    nb_proc_per_nodes = args.nb_proc_per_nodes
    output_traces_path = args.output_traces_path
    pathlib.Path(output_traces_path).mkdir(parents=True, exist_ok=True)

    input_tits = args.input_traces

    profiles = json.load(args.application_profiles_JSON)
    # make the profiles smpi batsim profiles
    for input_trace_path in input_tits:
        input_trace_dir: str = os.path.dirname(input_trace_path.name)
        input_trace_filename: str = os.path.basename(input_trace_path.name)
        relative_path = os.path.relpath(input_trace_dir, start=workloads_dir)
        app_name: str = os.path.splitext(os.path.basename(input_trace_path.name))[0]
        profile = profiles[app_name]
        profile["type"] = "smpi"
        profile["trace"] = f"{relative_path}/{input_trace_filename}"

    # Better pandas display
    pandas.options.display.float_format = "{:,.2f}".format
    all_calib_df = pandas.DataFrame()

    # put this in an async loop
    results = {}
    with concurrent.futures.ThreadPoolExecutor(
        max_workers=multiprocessing.cpu_count()
    ) as executor:
        for input_trace_path in input_tits:
            app_name: str = os.path.splitext(os.path.basename(input_trace_path.name))[0]
            results[app_name] = executor.submit(
                do_calibration_loop, app_name,
                os.path.abspath(input_trace_path.name), profiles[app_name]
            )

    for app, result in results.items():
        res = result.result()
        #print(f"{app}: result: {res}")
        all_calib_df =  all_calib_df.append(res, ignore_index=True)
    print(all_calib_df)
    calib_csv = "calibration.csv"
    all_calib_df.to_csv(calib_csv)
    print(f"Results exported to : {calib_csv}")
