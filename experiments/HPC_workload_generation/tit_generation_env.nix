let
  pkgs = import (builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-1809-2018-10-18";
    # Archive from commit hash
    url = https://github.com/nixos/nixpkgs/archive/09195057114a0a8d112c847a9a8f52957420857d.tar.gz;
    # Hash obtained using `nix-prefetch-url --unpack <url>` on the URL above
    sha256 = "0hszcsvgcphjny8j0p5inhl45ja61vjiz0csb0kx0b9lzmrafr7b";
  }) {};
in
  pkgs.buildEnv {
    name = "TIT_generation_env";

    paths = let
      simgrid = (pkgs.simgrid.override {fortranSupport = true;}).overrideAttrs (attrs: {
          # Use specific version of simgrid
          #src = pkgs.fetchFromGitHub {
          #  owner = "simgrid";
          #  repo = "simgrid";
          #  rev = "9043e9c63c0d478e75129e378279d6a84a491d9a";
          #  sha256 = "0lxmhyapbx252z194pxmydx67cc8hz3p1s5pw28d9x0yip0f9sv2";
          #};
          src = fetchGit {
            url = "https://framagit.org/simgrid/simgrid.git";
            rev = "9043e9c63c0d478e75129e378279d6a84a491d9a";
            ref = "master";
          };
          #src = fetchGit {
          #  url = "https://framagit.org/simgrid/simgrid.git";
          #  rev = "cfa37467445963796bfdafdec9bad8ec8566a60a";
          #  ref = "master";
          #};
        });
    in [
      simgrid
      pkgs.numactl
      pkgs.wget
    ];
  }
