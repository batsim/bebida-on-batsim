{
  kapack ? import (
    fetchTarball
    "https://github.com/oar-team/kapack/archive/master.tar.gz") {}
}:
with kapack;

pkgs.mkShell {
  buildInputs = [
    pkgs.glibcLocales
    ## for pajengr
    pkgs.flex
    pkgs.bison
    ##
    (pkgs.rstudioWrapper.override {
      packages = with pkgs.rPackages; [
        pkgs.pandoc
        pkgs.coreutils
        pkgs.which
        pkgs.R
        pkgs.unzip
        pkgs.gnutar
        Rcpp
        fmsb
        devtools
        GGally
        yaml
        optparse
        knitr
        rmarkdown
        plyr
        viridis
        stringi
        gtools
        rgl
        corrplot
        corrr
        readr
        tidyverse
        reshape
        reshape2
        dplyr
        ggplot2
        devtools
        network
        ggnetwork
      ];
    }
    )
    (python.withPackages (ps: with ps; with pythonPackages; [
      jupyter
      ipython
      kapack.evalys4
      aiofiles
    ]))
  ];
}
