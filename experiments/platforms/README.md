# Platforms

To generate a graphene platform with 33 nodes used the old graphene platform
smpi config values and copy the taurus platform structure to be able to add
storage nodes.

Install tools:
```sh
nix-env -iA nixos.libxml2
```

Then add nodes go from 16 to 33:
```py
import add_disks
from xml.etree.ElementTree import ElementTree, Element
from typing import List

tree = ElementTree()
root = tree.parse("./platform_graphene_s1.xml")
for i in range(17, 34):
    add_disks.add_host(root, '1.25E8Bps', '1.25E8Bps', "1.0E-4s", "16.673E9f",
            4, "graphene-"+str(i)+".nancy.grid5000.fr")

header = '''<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">'''
with open("./platform_graphene_s1_full.xml", 'wb') as f:
    f.write(header.encode('utf8'))
    tree.write(f, 'utf-8', xml_declaration=False)
```

Add the disk and format the output properly:
```sh
./add_disks.py platform_graphene_s1_full.xml DFS
cat output.xml | xmllint --format - > platform_graphene_33nodes_withDisks.xml
```

