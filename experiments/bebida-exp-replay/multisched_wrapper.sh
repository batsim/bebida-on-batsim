#!/usr/bin/env bash

set -x

trap 'kill -TERM $PID1 $PID2 $PID3' TERM INT

if [ -z "$1" ]; then echo Filesystem variant needed in argument; exit 1; fi

fs_variant=$1
bat_endpoint=${2:-"tcp://127.0.0.1:28000"}
hpc_endpoint=${3:-"tcp://127.0.0.1:28001"}
bda_endpoint=${4:-"tcp://127.0.0.1:28002"}

function exit_properly {
    wait -n # Wait of one of the process (Requires bash>=4.3)
    exit_code=$?
    if [ $exit_code -ne 0 ]
    then
        >&2 echo subprocess failed with exit code: $exit_code
        kill -TERM $PID1 $PID2 $PID3
        exit $exit_code
    fi
    wait
}

# run the broker that implement prolog/epilog and schedulers sync
batbroker -bat-endpoint $bat_endpoint -bda-endpoint $bda_endpoint -hpc-endpoint $hpc_endpoint &
PID1=$!
# run HPC scheduler
batsched -v conservative_bf -s $hpc_endpoint --verbosity quiet &
PID2=$!
# run BDA scheduler
pybatsim schedBebida -s $bda_endpoint --options "{\"variant\": \"$fs_variant\"}" --verbosity error &
PID3=$!

exit_properly
exit $exit_code
