#!/usr/bin/env python3
import json
import os
import glob

# Graphene cluster has 4 cores per node
nb_core_per_node = 4

for workload_file in glob.glob("bebida_irl_workloads/*hpc*.json"):
    with open(workload_file, 'r') as workload:
        wl = json.load(workload)
        # scale resource requests
        for job in wl["jobs"]:
            job["res"] = int(job["res"] / nb_core_per_node)
        wl["note"] = "the number of resourcves requested is converted from core to host, thus divided by 4 for the graphene cluster"
        wl["nb_res"] = int(wl["nb_res"] / nb_core_per_node)

        # exchange profiles from command to SMPI
        for p_name, profile in wl["profiles"].items():
            del profile["command"]
            del profile["delay"]
            profile["type"] = "smpi"
            profile["nb_requested_res"] = int(profile['np'] / nb_core_per_node)
            profile['trace'] = "traces/" + p_name

        # put this on a file
        with open("bebida_simul_workloads/" +
                os.path.splitext(os.path.basename(workload_file))[0] + '_hpc.json', 'w') as f:
            json.dump(wl, f, indent=4)

