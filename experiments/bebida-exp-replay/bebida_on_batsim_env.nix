let
  kapack = import (fetchTarball
    #https://github.com/oar-team/kapack/archive/master.tar.gz
    {
      name = "kapack-2019-01-16";
      # Archive from commit hash
      url = https://github.com/oar-team/kapack/archive/2abcea09317df1f85f537b21673650e25c7411df.tar.gz;
      sha256 = "0b9qbsab8mw4jj0kds3if3dmn768xhvg3f03kyvcjb5nhihp87ll";
    }
    ) {};
  inherit (kapack) pkgs;
in
  pkgs.buildEnv {
    name = "bebida_on_batsim_env";

    paths = let
      simgrid = kapack.simgrid_dev.overrideAttrs (oldAttrs: {
        #src = fetchGit {
        #  url = https://framagit.org/simgrid/simgrid.git;
        #  # master fail with double free of buffers
        #  # Use my tweak version to avoid double free on smpi_free_replay_tmp_buffers
        #  #rev = "9043e9c63c0d478e75129e378279d6a84a491d9a";
        #  # ^ fail because smpi_run api as changed
        #  #rev = "72dbb2eb04162e4023df83975da6b45c65f4403d";
        #  #rev = "57f4fe50e5dfac07ef2a9c9a69243c1571e359e2";
        #  # ^ Fail with osend on nullptr

        #  rev = "96e302254a70f19f236afce77529c08ad1fc9651";
        #  ref = "master";
        #};

        src = fetchGit {
            url = https://framagit.org/simgrid/simgrid.git;
            rev = "4f75ed0ce580c492d6a9417cfbb5187d2e62fbca";
            ref = "master";
        };
        # Re apply my fix to avoid double free
        patches = [./0001-smpi-use-maps-on-actor-to-use-one-buffer-for-each.patch];
        #src = /home/mmercier/Projects/simgrid;
      });
      batsim = (kapack.batsim_dev.override {
          inherit simgrid;
        }
      );#.overrideAttrs (oldAttrs: {
      #  #src = fetchGit {
      #  #  url = https://framagit.org/batsim/batsim.git;
      #  #  rev = "65bf01eb2a6249bda1e2a436e038dcb7e4381eee";
      #  #  ref = "master";
      #  #};
      #});
      pybatsim = kapack.pybatsim.overrideAttrs (oldAttrs: {
        src = fetchGit {
          url = https://gitlab.inria.fr/batsim/pybatsim.git;
          rev = "f1d5bd44bc3612bfd75e67f0d779cb8801f97a3e";
          ref = "master";
        };
        propagatedBuiltInputs = oldAttrs.propagatedBuildInputs ++ [ kapack.procset ];
      });
      #pybatsim = kapack.pybatsim_dev.overrideAttrs (oldAttrs: {
      #  src = ~/Projects/pybatsim;
      #});
      #batsched = kapack.batsched_dev.overrideAttrs (oldAttrs: {
      #  src = fetchGit {
      #    url = https://gitlab.inria.fr/batsim/batsched.git;
      #    rev = "39a30d83103ae3569893fcaf9a31e58ebf7cae22";
      #    ref = "master";
      #  };
      #});

    in [
      #kapack.batsim_dev
      batsim
      #kapack.pybatsim
      pybatsim
      kapack.batsched
      kapack.batbroker
      kapack.batexpe
    ];
  }
